package by.sc.exchangeapin.controller;

import by.sc.exchangeapin.domain.User;
import by.sc.exchangeapin.dto.LoginDto;
import by.sc.exchangeapin.service.AuthenticationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private AuthenticationService authenticationService;

    @ApiOperation(value = "Логин пользователя в систему")
    @PostMapping(value = "/login")
    public String login(@RequestBody LoginDto dto) {
        User user = (User) authenticationService.loadUserByUsername(dto.getLogin());
        return authenticationService.login(user, dto);
    }
}
