package by.sc.exchangeapin.controller;

import by.sc.exchangeapin.dto.MsgDto;
import by.sc.exchangeapin.service.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/message")
public class MessageController {

    @Autowired
    private SendService sendService;

    @PostMapping
    public void sendMessage(@RequestBody MsgDto dto) {
        sendService.send(dto);
    }
}
