package by.sc.exchangeapin.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class MsgDto {
    private int id;
    private String sourceCurrency;
    private String targetCurrency;
    private BigDecimal amount;
    private String email;
}
