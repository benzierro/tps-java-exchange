package by.sc.exchangeapin.service;

import by.sc.exchangeapin.domain.User;
import by.sc.exchangeapin.dto.LoginDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthenticationService extends UserDetailsService {
    void replaceUserInCache(String token, User user);

    String login(User user, LoginDto loginDto);
}
