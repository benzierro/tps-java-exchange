package by.sc.exchangeapin.service;


import by.sc.exchangeapin.dto.MsgDto;

public interface SendService {
    void send(MsgDto dto);
}
