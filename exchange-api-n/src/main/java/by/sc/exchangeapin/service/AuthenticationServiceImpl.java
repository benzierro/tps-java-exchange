package by.sc.exchangeapin.service;

import by.sc.exchangeapin.domain.User;
import by.sc.exchangeapin.dto.LoginDto;
import by.sc.exchangeapin.config.jwt.JwtTokenProvider;
import by.sc.exchangeapin.repository.UserRepository;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.replicatedmap.ReplicatedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = "users")
@Transactional
public class AuthenticationServiceImpl implements by.sc.exchangeapin.service.AuthenticationService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    @Qualifier("hazelcastInstance")
    private HazelcastInstance hazelcastInstance;

    private static final String CACHE_NAME = "users";

    @Cacheable
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsernameEquals(s);
    }

    @Override
    public void replaceUserInCache(String token, User user) {
        ReplicatedMap<Object, Object> map = hazelcastInstance.getReplicatedMap(CACHE_NAME);

        user.setPreviousToken(token);

        map.put(user.getUsername(), user, 60, TimeUnit.MINUTES);

    }

    @Override
    public String login(User user, LoginDto loginDto) {
        if (!passwordEncoder.matches(loginDto.getPassword(), user.getPassword())) {
            throw new RuntimeException();
        }

        String token = jwtTokenProvider.createToken(user.getUsername(), user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));

        var now = LocalDateTime.now();
        user.setLoginDate(now);
        user.setPreviousToken(token);
        replaceUserInCache(token, user);

        return token;
    }
}
