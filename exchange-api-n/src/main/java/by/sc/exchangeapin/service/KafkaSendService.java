package by.sc.exchangeapin.service;


import by.sc.exchangeapin.dto.MsgDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSendService implements SendService {

    @Autowired
    private KafkaTemplate<Long, MsgDto> kafkaTemplate;

    @Override
    public void send(MsgDto dto) {
        kafkaTemplate.send("exchange",dto);
    }
}
