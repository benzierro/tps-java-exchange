package by.sc.exchangeapin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExchangeApiNApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExchangeApiNApplication.class, args);
    }

}
