package by.sc.exchangescheduler.repository;

import by.sc.exchangescheduler.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
