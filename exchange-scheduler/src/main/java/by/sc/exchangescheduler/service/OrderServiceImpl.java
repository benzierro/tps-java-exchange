package by.sc.exchangescheduler.service;

import by.sc.exchangescheduler.domain.Order;
import by.sc.exchangescheduler.dto.OrderDto;
import by.sc.exchangescheduler.repository.CurrencyRepository;
import by.sc.exchangescheduler.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@CacheConfig(cacheNames = "orders")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Cacheable
    @Override
    public List<OrderDto> findAll() {

        System.out.println(SecurityContextHolder.getContext().getAuthentication());

        return orderRepository.findAll().stream()
                .map(order -> {
                    var dto = new OrderDto();
                    dto.setAmount(order.getAmount());
                    dto.setId(order.getId());
                    dto.setSourceCurrency(order.getSourceCurrency().getCode());
                    dto.setTargetCurrency(order.getTargetCurrency().getCode());
                    return dto;
                }).collect(Collectors.toList());
    }

    @Override
    public void save(OrderDto dto) {
        var order = new Order();

        order.setAmount(dto.getAmount());

        currencyRepository.findByCode(dto.getSourceCurrency()).ifPresent(
                order::setSourceCurrency
        );

        currencyRepository.findByCode(dto.getTargetCurrency()).ifPresent(
                order::setTargetCurrency
        );


        orderRepository.save(order);
    }

    @Override
    public void deleteById(int id) {
        orderRepository.deleteById(id);
    }
}
