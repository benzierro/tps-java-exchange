package by.sc.exchangescheduler.service;


import by.sc.exchangescheduler.dto.OrderDto;

import java.util.List;

public interface OrderService {
    List<OrderDto> findAll();

    void save(OrderDto dto);

    void deleteById(int id);
}
