package by.sc.exchangescheduler;


import by.sc.exchangescheduler.dto.MsgDto;
import by.sc.exchangescheduler.dto.OrderDto;
import by.sc.exchangescheduler.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaMsgReceiveServiceImpl {

    private static EmailSender sslSender = new EmailSender("hierrof09@gmail.com", "1234nji");
    @Autowired
    private OrderService orderService;

    @KafkaListener(id = "1234", topics = {"exchange"}, containerFactory = "singleFactory")
    public void consume(MsgDto dto) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(dto.getId());
        orderDto.setAmount(dto.getAmount());
        orderDto.setSourceCurrency(dto.getSourceCurrency());
        orderDto.setTargetCurrency(dto.getSourceCurrency());

        orderService.save(orderDto);
        sslSender.send("Order", "order was created", "hierrof09@gmail.com", dto.getEmail());
    }
}
