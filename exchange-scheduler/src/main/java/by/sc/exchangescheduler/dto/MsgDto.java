package by.sc.exchangescheduler.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class MsgDto {
    private int id;
    private String sourceCurrency;
    private String targetCurrency;
    private BigDecimal amount;
    private String email;
}
