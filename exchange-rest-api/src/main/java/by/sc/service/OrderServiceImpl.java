package by.sc.service;

import by.sc.dto.OrderDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Override
    public void requestCreation(OrderDto dto) {
        System.out.println(dto.toString());
    }

}
