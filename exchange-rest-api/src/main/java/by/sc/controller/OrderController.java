package by.sc.controller;

import by.sc.dto.OrderDto;
import by.sc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/add")
    public void addOrder(@RequestBody OrderDto dto) {
        orderService.requestCreation(dto);
    }
}
